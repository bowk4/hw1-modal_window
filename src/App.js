import React, { useState } from 'react';
import Button from "./components/Button";
import Modal from "./components/Modal";

const App = () => {
    const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
    const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

    const openFirstModal = () => {
        setIsFirstModalOpen(true);
    };

    const openSecondModal = () => {
        setIsSecondModalOpen(true);
    };

    const closeFirstModal = () => {
        setIsFirstModalOpen(false);
    };

    const closeSecondModal = () => {
        setIsSecondModalOpen(false);
    };

    return (
        <div>
            <Button backgroundColor="red" text="Delete" onClick={openFirstModal} />
            <Button backgroundColor="green" text="Save" onClick={openSecondModal} />

            {isFirstModalOpen && (
                <Modal
                    backgroundModal="tomato"
                    header="Dou you want to delete this file?"
                    closeButton={true}
                    text="Once delete this file, It won't be possible to undo this action.
                    Are you sure you want to delete it?"
                    actions={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Cancel" onClick={closeFirstModal} />}
                    action={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Ok" onClick={closeFirstModal} />}
                    onClose={closeFirstModal}
                />
            )}

            {isSecondModalOpen && (
                <Modal
                    backgroundModal="seagreen"
                    header="Dou you want to save this file?"
                    closeButton={true}
                    text="After saving this file, you can download it at any time"
                    actions={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Close" onClick={closeSecondModal} />}
                    action={<Button backgroundColor="rgba(0, 0, 0, 0.2)" text="Save" onClick={closeSecondModal} />}
                    onClose={closeSecondModal}
                />
            )}
        </div>
    );
};

export default App;
